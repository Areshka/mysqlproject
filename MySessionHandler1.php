<?php

class MySessionHandler implements SessionHandlerInterface
{

    public function open($savePath, $sessionName) {
        $host = '192.168.114.128';
        $user = 'root';
        $pass = 'pass@word1MINT';
        $name = 'save_session_in_db';
        $this->db = new PDO("mysql: host=" . $host . "; dbname=" . $name, $user, $pass);
        return true;

    }

    public function close() {
        $this->db = null;
        return true;
    }

    public function read($id) {
        if (!isset($this->read_db)) {
            $this->read_db = $this->db->prepare("SELECT name FROM session WHERE id = :id");
        }
        $this->read_db->bindValue(':id', $id);
        $this->read_db->execute();
        while ($row = $this->read_db->fetch()) {
            echo $row . "<br>";
        }
    }

    public function write($id, $data) {
        if (!isset($this->write_db)) {
            $this->write_db = $this->db->prepare("INSERT INTO session (name) VALUES (:name) ");
        }
        $this->write_db->bindValue(':name', $data);
        $this->write_db->execute();
        return true;
    }

    public function destroy($id) {
        if (!isset($this->delete_db)) {
            $this->delete_db = $this->db->prepare("DELETE FROM session WHERE id = :id");
        }
        $this->delete_db->bindValue(':id', $id);
        $this->delete_db->execute();
        return true;
    }

    public function gc($maxlifetime) {
        return true;
    }
}

$handler = new MySessionHandler;
session_set_save_handler($handler, true);

