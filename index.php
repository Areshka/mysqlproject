<?php
session_start();
if (!$_SESSION['time']) {
    $_SESSION['ua'] = $_SERVER['HTTP_USER_AGENT'];
    $_SESSION['remote_addr'] = $_SERVER['REMOTE_ADDR'];
    $_SESSION['time'] = date("H:i:s");
}

if ($_SESSION['ua'] != $_SERVER['HTTP_USER_AGENT']) {
   die('Wrong browser');
}
if ($_SESSION['remote_addr'] != $_SERVER['REMOTE_ADDR']) {
    unset($_SESSION['remote_addr']);
}

echo $_SESSION['time'];
?>
